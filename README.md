# moon_becomes_saturn

This texture pack is essentially a texture pack version of a zip Twoelk posted [here](https://forum.minetest.net/viewtopic.php?p=150727#p150727). The credit goes to Twoelk, not me.

I have resurrected Twoelk's work because I see the potential of the saturn texture for my mods.

Media is licenced as CC0.

Information from info.txt from Twoelk's zip:
> The original picture of Saturn can be found on Wikimedia under the following url:
http://commons.wikimedia.org/wiki/File:Saturn_from_Cassini_Orbiter_-_Square_(2004-10-06).jpg

> The orginal picture was made by the NASA and seems to be public domain.
All changes I (twoelk) made to the file may be considered licensed as CC0.